#!/bin/sh

apk add --no-cache py3-pip && pip3 install --upgrade pip

pip install awscli 

echo
echo "- Creating table... (Ignore preexisting error, if any)"
aws dynamodb create-table \
  $AWS_DYNAMODB_ENDPOINT_ARGS \
  --table-name traefik \
  --attribute-definitions AttributeName=id,AttributeType=S \
  --key-schema AttributeName=id,KeyType=HASH \
  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

echo
echo "- Awaiting table creation..."
aws dynamodb wait table-exists \
  $AWS_DYNAMODB_ENDPOINT_ARGS \
  --table-name=traefik

echo
echo "- Inserting backend entry..."
aws dynamodb put-item \
  $AWS_DYNAMODB_ENDPOINT_ARGS \
  --table-name traefik \
  --return-consumed-capacity TOTAL \
  --item '
  {
    "id": {"S": "minds-pro-backend"},
    "name": {"S": "minds-pro"},
    "backend": {"M": {
      "servers": {"M": {
        "minds-com": {"M": {
          "url": {"S": "'$MINDS_COM_ENDPOINT'"}
        }}
      }}
    }}
  }
'

echo
echo "Done!"

traefik