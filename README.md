# Minds Pro Routing

## Local development setup

1- Modify `docker-compose.dev.yml` to suit your Minds environment

2- (re-)Build and run services
```
$ docker-compose -f docker-compose.yml -f docker-compose.dev.yml build

$ docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
```

3- Visit http://localhost:8080

### Notes

* Minds should be running on another port different than `80` or `8080`. This should be reflected in `MINDS_COM_ENDPOINT` environment at `.dev.yml` file.

* You might want to disable (temporarily) `SSLRedirect` in `engine/Core/Pro/Domain/EdgeRouters/TraefikDynamoDb.php`.

* `[pro][dynamoDbEndpoint]` in `settings.php` should point to `http://YOUR_DOCKER_HOST_IP_ADDRESS:8000`.